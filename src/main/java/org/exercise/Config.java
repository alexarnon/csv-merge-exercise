package org.exercise;

import java.io.File;

public class Config {
    public String path;
    public File file;
    public int keyIndex;
    public int maxRecords;
    public boolean debug;
    public boolean sequential = true;

    public Config(String[] args) {
        this.path = args[0];
        this.file = new File(args[0]);
        this.keyIndex = Integer.parseInt(args[1]);
        this.maxRecords = Integer.parseInt(args[2]);
        this.debug = args.length > 3 && args[3].equals("debug");
    }

    @Override
    public String toString() {
        return "Config{" +
                "path='" + path + '\'' +
                ", file=" + file +
                ", keyIndex=" + keyIndex +
                ", maxRecords=" + maxRecords +
                ", debug=" + debug +
                ", sequential=" + sequential +
                '}';
    }
}
