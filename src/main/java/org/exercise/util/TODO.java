package org.exercise.util;

public class TODO extends RuntimeException {

    public static <T> T a(String what) {
        throw new TODO(what);
    }

    public TODO(String what) {
        super("not implemented: " + what);
    }

}
