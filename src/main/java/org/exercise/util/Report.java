package org.exercise.util;

import java.io.IOException;

public class Report {

    public static void info(String fmt, Object ... arg) {
        System.err.format("INFO: " + fmt + "\n", arg);
    }

    public static void error(String fmt, Object ... arg) {
        System.err.format("ERROR: " + fmt + "\n", arg);
    }

    public static void dieOnException(ThrowingProcedure procedure) {
        try {
            procedure.run();
        } catch (Throwable ioe) {
            System.err.println("exiting due to exception");
            ioe.printStackTrace();
            System.out.flush();
            System.err.flush();
            System.exit(1);
        }
    }

    public static void dieOnException(Runnable procedure) {
        try {
            procedure.run();
        } catch (Throwable ioe) {
            System.err.println("exiting due to exception");
            ioe.printStackTrace();
            System.out.flush();
            System.err.flush();
            System.exit(1);
        }
    }
}
