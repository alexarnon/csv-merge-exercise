package org.exercise.util;

import java.io.IOException;

@FunctionalInterface
public interface ThrowingProcedure {
    void run() throws Exception;
}
