package org.exercise;

import org.exercise.csv.CsvHandling;
import org.exercise.csv.CsvRow;
import org.exercise.merging.Merger;
import org.exercise.util.Report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {

    /**
     * Arguments:
     * <il>
     * <li>File path</li>
     * <li>Key field index (1-based)</li>
     * <li>Max records in memory</li>
     * <li>Optional: "debug" to prevent deletion of temporary storage</li>
     * </il>
     * [TODO: nice error handling, usage report, proper argument names etc.]
     */
    public static void main(String[] args) throws IOException {
        Config config = new Config(args);
        File fragmentsDirPath = createInitialPartition(config);
        File fromDir, toDir;
        for (fromDir = fragmentsDirPath, toDir = createNextPartition(fromDir) ;
             toDir != null ;
             fromDir = toDir, toDir = createNextPartition(fromDir)) {
            // A new Merger is created and torn down per dir/partition, to simplify halt synchronization.
            Merger merger = Merger.newMerger(toDir, config);
            for (File[] files: allFilePairs(fromDir)) {
                if (files[1] == null) {
                    Files.copy(new File(fromDir, files[0].getName()).toPath(), new File(toDir, files[0].getName()).toPath());
                } else {
                    File f1 = new File(fromDir, files[0].getName());
                    File f2 = files[1] == null ? null : new File(fromDir, files[1].getName());
                    merger.scheduleMerge(f1, f2, toDir);
                }
            }
            merger.shutdown();
            merger.awaitShutdown();
            removeSrcDir(fromDir, config.debug);
        }
        // We are done, a single file remains!
        Path lastResult = findSingleFile(fromDir);
        Path finalFile = new File(config.path + ".sorted").toPath();
        Report.info("copying last file: %s -> %s", lastResult, finalFile);
        Files.copy(lastResult, finalFile);
        removeSrcDir(fromDir, config.debug);
    }

    private static File createInitialPartition(Config config) throws IOException {
        File rootDir = config.file.getParentFile();
        File initialPartitionDir = new File(rootDir, config.file.getName() + "_0");
        if (! initialPartitionDir.mkdir()) throw new RuntimeException("unable to create initial partition: " + initialPartitionDir);
        int[] n = {0};
        CsvHandling.withBlocks(config.path, config.maxRecords, rows -> {
            try (FileWriter writer = new FileWriter(new File(initialPartitionDir, "block_"+n[0]++))) {
                Report.info("writing initial block of %d rows", rows.size());
                List<CsvRow> sortedRows = rows.stream()
                        .sorted(CsvHandling.rowComparator(config.keyIndex - 1))
                        .collect(Collectors.toList());
                CsvHandling.writeBlock(writer, sortedRows);
                writer.flush();
            }
        });
        return initialPartitionDir;
    }

    /** Create next partition directory, if prev directory has more than one file. */
    @SuppressWarnings("ConstantConditions")
    public static File createNextPartition(File prevDir) {
        if (prevDir.list().length < 2) return null;
        File rootDir = prevDir.getParentFile();
        File nextDir = new File(rootDir, prevDir.getName() + "_x");
        if (! nextDir.mkdir()) throw new RuntimeException("unable to create next partition: " + nextDir);
        return nextDir;
    }

    @SuppressWarnings("ConstantConditions")
    public static List<File[]> allFilePairs(File dirPath) {
        String[] list = dirPath.list();
        List<File[]> pairs = new ArrayList<>();
        for (int i = 0 ; i < list.length ; i += 2) {
            File[] pair = { new File(list[i]), i == list.length - 1 ? null : new File(list[i + 1]) };
            pairs.add(pair);
        }
        return pairs;
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
    private static void removeSrcDir(File dir, boolean debug) {
        if (debug) {
            Report.info("skipping source dir delete: %s", dir);
        } else {
            Report.info("deleting source dir: %s", dir);
            for (String filename: dir.list()) new File(dir, filename).delete();
            dir.delete();
        }
    }

    private static Path findSingleFile(File dirPath) {
        String[] list = dirPath.list();
        if (list == null || list.length != 1) {
            throw new IllegalArgumentException("single-file directory expected: " + dirPath);
        } else {
            return new File(dirPath, list[0]).toPath();
        }
    }
}
