package org.exercise.csv;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CsvRow {

    private final List<String> fields;

    public CsvRow(List<String> fields) {
        if (fields == null || fields.size() != 3 || fields.stream().anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("bad field(s): " + fields);
        }

        this.fields = List.copyOf(fields);
    }

    public CsvRow(String line) {
        this(Arrays.stream(line.split(","))
                .map(String::trim)
                .collect(Collectors.toList()));
    }

    public String toCsv() {
        return String.join(",", fields);
    }

    public List<String> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "CsvRow{fields=" + fields + '}';
    }
}
