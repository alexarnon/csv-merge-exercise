package org.exercise.csv;

import org.exercise.util.IOConsumer;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CsvHandling {

    public static Comparator<CsvRow> rowComparator(int fieldIndex) {
        return Comparator.comparing(row -> row.getFields().get(fieldIndex));
    }

    public static void withBlocks(String path, int nRows, IOConsumer<List<CsvRow>> action) throws IOException {
        // Note: Using BufferedReader for its ability to easily split lines - I know it's a potential bit of cheating due to buffer.
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            for (List<CsvRow> block = readBlock(reader, nRows) ; ! block.isEmpty() ; block = readBlock(reader, nRows)) {
                action.accept(block);
            }
        }
    }

    public static List<CsvRow> readBlock(BufferedReader reader, int nRows) throws IOException {
        if (nRows <= 0) {
            return new ArrayList<>();
        } else {
            ArrayList<CsvRow> ret = new ArrayList<>(nRows);
            for (int i = 0 ; i < nRows ; i++) {
                CsvRow row = readRow(reader);
                if (row == null) {
                    break;
                } else {
                    ret.add(row);
                }
            }
            return ret;
        }
    }

    public static void iterSortedFiles(File f1, File f2, Comparator<CsvRow> comparator, IOConsumer<CsvRow> nextRowAction) throws IOException {
        try (BufferedReader reader1 = new BufferedReader(new FileReader(f1));
             BufferedReader reader2 = new BufferedReader(new FileReader(f2))) {
            CsvRow row1 = readRow(reader1);
            CsvRow row2 = readRow(reader2);
            while (row1 != null || row2 != null) {
                if (row1 == null || row2 != null && comparator.compare(row1, row2) > 0) {
                    nextRowAction.accept(row2);
                    row2 = readRow(reader2);
                } else if (row2 == null || comparator.compare(row1, row2) <= 0) {
                    nextRowAction.accept(row1);
                    row1 = readRow(reader1);
                } else {
                    throw new IllegalStateException("missed something here");
                }
            }
        }
    }

    private static CsvRow readRow(BufferedReader reader) throws IOException {
        String line = reader.readLine();
        return line == null ? null : new CsvRow(line);
    }

    public static void mergeFiles(File f1, File f2, Comparator<CsvRow> comparator, File dstDir) throws IOException {
        String nextFileName = f1.getName()/* + "_m"*/;
        if (f2 == null) {
            Files.copy(f1.toPath(), new File(dstDir, f1.getName()).toPath());
        } else {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(dstDir, nextFileName)))) {
                iterSortedFiles(f1, f2, comparator, row -> writer.write(row.toCsv() + "\n"));
                writer.flush();
            }
        }
    }

    public static void writeBlock(Writer writer, List<CsvRow> rows) throws IOException {
        for (CsvRow row : rows) writer.write(row.toCsv() + "\n");
    }
}
