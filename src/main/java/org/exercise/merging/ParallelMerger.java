package org.exercise.merging;

import org.exercise.Config;
import org.exercise.util.TODO;

import java.io.File;

public class ParallelMerger implements Merger {

    public ParallelMerger(Config config) {
        TODO.a("ParallelMerge");
    }

    @Override
    public void shutdown() {
        TODO.a("ParallelMerge.shutdown()");
    }

    @Override
    public void awaitShutdown() {
        TODO.a("ParallelMerge.awaitShutdown()");
    }

    @Override
    public void scheduleMerge(File f1, File f2, File dstDir) {
        TODO.a("ParallelMerge.scheduleMerge()");
    }
}
