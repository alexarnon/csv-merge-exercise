package org.exercise.merging;

import org.exercise.Config;

import java.io.File;

public interface Merger {

    static Merger newMerger(File outputDir, Config config) {
        return config.sequential ? new SequentialMerger(config) : new ParallelMerger(config);
    }

    void shutdown();

    void awaitShutdown();

    void scheduleMerge(File f1, File f2, File dstDir);
}


