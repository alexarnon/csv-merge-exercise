package org.exercise.merging;

import org.exercise.Config;
import org.exercise.csv.CsvHandling;
import org.exercise.csv.CsvRow;
import org.exercise.util.Report;
import org.exercise.util.ThrowingProcedure;

import java.io.File;
import java.util.Comparator;

import static org.exercise.util.Report.dieOnException;

public class SequentialMerger implements Merger {

    private final Comparator<CsvRow> comparator;

    public SequentialMerger(Config config) {
        this.comparator = CsvHandling.rowComparator(config.keyIndex - 1);
    }

    @Override
    public void shutdown() {
        Report.info("(nil shutdown)");
    }

    @Override
    public void awaitShutdown() {
        Report.info("(nil await shutdown)");
    }

    @Override
    public void scheduleMerge(File f1, File f2, File dstDir) {
        dieOnException((ThrowingProcedure) () -> CsvHandling.mergeFiles(f1, f2, comparator, dstDir));
    }
}
