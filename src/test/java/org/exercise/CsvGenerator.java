package org.exercise;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.util.Random;
import java.util.stream.IntStream;

public class CsvGenerator {

    public static void main(String[] unused) {
        Random random = new SecureRandom();
        int nRows = 1000;
        try (PrintWriter writer = new PrintWriter(new FileWriter(AppTest.TMP_PATH+"/input.csv"))) {
            IntStream.range(0, nRows).forEach(i -> writer.format("%s,%08x,%s\n", i, random.nextLong(), random.nextInt()));
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException("unable to create/write test file", e);
        }
    }

}
