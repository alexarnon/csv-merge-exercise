# README #

Hi, this is the CSV merging exercise. 
At this point parallel merging is not done, though the code is ready for it...

No real error handling is done, and the codebase is a bit of a quick mess.

With the following run configurations you can see `input.csv` in the `tmp` directory being merged.
Note that before running again, the result and any intermediate data need to be deleted.

Just the merge + result: `tmp/input.csv 2 10`

Keeping intermediate files: `tmp/input.csv 2 10 debug`

To generate a new input file, please run `CsvGenerator` and copy from the test resources dir.
